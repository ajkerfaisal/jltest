//
//  AppDelegate.swift
//  JLTest
//
//  Created by Mohammad.Faisal on 2/10/18.
//  Copyright © 2018 Faisal LLC. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

